/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "math.h"


#define TRANSMIT_BUFFER_SIZE  64//used o send uart messages
#define PARAMETERSIZE 7
char TransmitBuffer[TRANSMIT_BUFFER_SIZE];//We need this to interface with the API PWM calls and to output it to the UART
char parameterarray[PARAMETERSIZE];//This is what I will use to parse the sent commands. I am creating an array of Uint16. 
                                //let's say the gui sends in "u200" then I will store the "200" in this array as 000200
//volatile uint8_t interruptcounter = 0;


int    parameter=0;
uint8  loopcounter=0;
char   command=0; //This is similar to the parameter variable. In here I will store the command the user has sent.
char   Ch;        /* Variable to store UART received character */
char   tempchar;
char tempcommand = 0;
volatile uint8 interruptcounter=0; //This is used to fill the parameter array

void PrintInt(int value) //This function takes in an integer and puts it on the UART.How do I pass a string to this?
{
    UART_1_PutString("\nYour integer is: ");
    sprintf(TransmitBuffer,"%d", value);
    UART_1_PutString(TransmitBuffer);
    UART_1_PutString("\n");
}

int ConvertParamCharToUint()//This function sorts through the parameter array and subtracts off 48. For example, if the user sent "u200", then the
                            //200 in ascii is actually 50 48 48
{
    int fullnum=0;
    for(int i=0;i<=PARAMETERSIZE-1;i++)
    {
       fullnum=fullnum+(parameterarray[i]-48)*pow(10,i);//This looks complicated, but I am just trying to build a number out of the entered characters. 
    }                                           //on iteration 1, I am in the ones place, so I just want to multiple by 1. On the second iteration I 
                                               //am on the 10s digit so I want to multiply by 10^1 or 10. If I sum as I go, I'll rebuild the number
    return fullnum;
}

void ClearParameterArray() //all this function does is zero out the parameter array. 
{
    for (int i=0; i<PARAMETERSIZE;i++)
    {
        parameterarray[i]=48;//This is zero in ascii or char world. So subtract off 48 to get the actual #
    }
    
}

CY_ISR(isrpointer)
{
    tempchar=UART_1_GetChar();  //On the interrupt grab a char
   // UART_1_PutChar(tempchar);
    if(interruptcounter==0)
    {  
        tempcommand = tempchar;
        interruptcounter++;
    }
    else if (tempchar=='*')
        {
            interruptcounter=0;
            command=tempcommand;    //  I will use the command variable in my main.c case statement 
                                //  I don't want it to execute until the parameter array is filled
        }
         else
        {
        parameterarray[PARAMETERSIZE-interruptcounter]=tempchar;

        interruptcounter++;
        }
   
}

int main(void)
{
    CyGlobalIntEnable;
    ClearParameterArray();
    /* Start the components */
    UART_1_Start(); //UART is the communications protocol between my dev board and a python gui. 
                    /* Send message to verify COM port is connected properly */
    UART_1_PutString("COM Port Open\n");
   
    isr_1_StartEx(isrpointer);//This starts the Isr1 interrupt and you need a pointer to CY_ISR(pointer) function

    for(;;)
    {
        switch(command)
        {
            case 0:
                break;
                case 'a':
                    parameter= ConvertParamCharToUint();
                    ClearParameterArray();
                    PrintInt(parameter);
                    command=0;
                    interruptcounter=0;//why do I need this? I thought I was changing this is the interrupt routine itself.
                    
                break;
                case 'b':
                    UART_1_PutString("\nThere is no parameter to parse here!\n");
                    command=0;
                    interruptcounter=0;
                break;        
                case 'c':
                break;
                case 'd':
                break;
                case 'e':
                break;
                case 'f':
                break;
                case 'g':
                break;
                case 'h':
                break;
                case 'i':
                break;
        
        }
        
        
       
    }
}

/* [] END OF FILE */
